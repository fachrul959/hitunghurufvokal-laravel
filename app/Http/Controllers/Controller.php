<?php

namespace App\Http\Controllers;

use App\Models\Model;
use Illuminate\Http\Request;

class Controller
{
    public function index()
    {
        return view('home');
    }

    public function store(Request $request)
    {
        $arr = $request->all();
        Model::setVowels($arr['text']);
        return view('result', [
            "vowels" => Model::getVowels(),
            "counts" => Model::getCounts()
        ]);
    }
}
