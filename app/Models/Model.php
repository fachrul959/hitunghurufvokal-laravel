<?php

namespace App\Models;


// use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;

class Model
{
    private static $count;
    private static $vowels = [];

    public static function setVowels($word)
    {
        $chars = str_split($word);
        $arr = array_filter($chars, function ($char) {
            if ($char == 'a' | $char == 'i' | $char == 'u' | $char == 'e' | $char == 'o') {
                return TRUE;
            } else {
                return FALSE;
            }
        });

        self::$vowels = array_unique($arr);
        self::$count = count(self::$vowels);
    }

    public static function getVowels()
    {
        return self::$vowels;
    }

    public static function getCounts()
    {
        return self::$count;
    }
}
