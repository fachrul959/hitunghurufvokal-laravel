@extends('layout.main')
@section('container')
<div class="container d-flex mt-4 flex-column align-items-center">
    <h1 class="mb-3">Hitung Huruf Vokal</h1>
    <p>Masukkan Teks atau Kata</p>
    <form action="/result" method="post" class="d-flex flex-column align-items-center">
        @csrf
        <div class="input-group">
            <textarea class="form-control" id="text-area" name="text" aria-label="With textarea"></textarea>
        </div>
        <input type="submit" class="btn btn-outline-secondary mt-3" value="Submit">
    </form>
</div>
@endsection
