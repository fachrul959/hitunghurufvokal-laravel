@extends('layout.main')
@section('container')
<div class="container d-flex mt-4 flex-column align-items-center">
    <h1 class="mb-3">Hasil</h1>
    @if ($counts !== 0)
    <p>Terdapat {{ $counts }} huruf vokal yaitu @foreach ($vowels as $vowel){{ $vowel }} @endforeach</p>
    @else
    <p>Tidak ada huruf vokal</p>
    @endif
</div>
@endsection
